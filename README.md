# rest-template
---
Template for REST development

## Environment

* Java 6
* JBoss EAP 6.x [download](http://www.jboss.org/jbossas/downloads/)
* Git v1.9.x. [download](http://git-scm.com/downloads/)
* Maven v3.x.x. [download](http://maven.apache.org/download.cgi)

## System Variables
```
MAVEN_HOME = c:\Maven\apache-maven-3.2.1
JBOSS_HOME = C:\jboss-eap-6.1
GIT_HOME = C:\Program Files (x86)\Git
JAVA_HOME = C:\Program Files\Java\jdk1.6.0_45
Path = %MAVEN_HOME%\bin;%JAVA_HOME%\bin;%GIT_HOME%\bin
```

## Clone Source Code
```
> git clone https://bitbucket.org/hazizurin/rest-template.git
```

## Build, Package & Deploy
With JUnit:
```
> mvn package
```
or without JUnit:
```
> mvn package -DskipTests
```
clean-up & package:
```
> mvn clean package -DskipTests
```

## Start JBoss

1. Start JBoss `<JBOSS_HOME>/bin/standalone.bat`
2. Goto URL: <http://localhost:8080>
3. Goto URL: <http://localhost:8080/rest-template>

## Run Unit Test
```
> mvn test
```

## Maven Project Structure
```
pom.xml

/src/main/java

         /webapp

         /resources

/src/test/java

         /resources
```
## REST Path Structure
```
URL: http://localhost:8080/<war-filename>/<application-path>/<resource-class-path>/<method-path>
```

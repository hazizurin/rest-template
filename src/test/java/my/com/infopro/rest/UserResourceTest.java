/**
 * 
 */
package my.com.infopro.rest;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.representation.Form;

/**
 * @author Asil
 *
 */
public class UserResourceTest {
	static WebResource webResource;
	static ClientResponse clientResponse;

	static String uriBase = "http://localhost:8080/rest-template/rest";

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ClientConfig clientConfig = new DefaultClientConfig();
		Client client = Client.create(clientConfig);
		
		webResource = client.resource(uriBase);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test return text
	 */
	@Test
	public void testUser() {
		clientResponse = webResource.path("user")
				.get(ClientResponse.class);			

		System.out.println("response: " + clientResponse.getEntity(String.class));
		assertEquals(Response.Status.OK.toString(), clientResponse.getStatusInfo().toString());
	}	

	/**
	 * Test return JSON
	 */
	@Test
	public void testUserProfile() {
		clientResponse = webResource.path("user/profile")
				.accept(MediaType.APPLICATION_JSON)
				.get(ClientResponse.class);			

		System.out.println("response: " + clientResponse.getEntity(String.class));
		assertEquals(Response.Status.OK.toString(), clientResponse.getStatusInfo().toString());
	}	

	/**
	 * Test post JSON
	 */
	@Test
	public void testRegister() throws JSONException {
		JSONObject dateJSON = new JSONObject();
		
		dateJSON.put("username", "Tony");
		dateJSON.put("location", "Sunway");

		clientResponse = webResource.path("user/register")
				.accept(MediaType.APPLICATION_JSON)
				.type(MediaType.APPLICATION_JSON)
				.entity( dateJSON )
				.post(ClientResponse.class);

		System.out.println("response: " + clientResponse.getEntity(String.class));
		assertEquals(Response.Status.OK.toString(), clientResponse.getStatusInfo().toString());
	}

	/**
	 * Test send query
	 */
	@Test
	public void testSendMessage() {
		clientResponse = webResource.path("user/age")			
				.queryParam("birth", "28/02/1970")
				.accept(MediaType.TEXT_PLAIN)
				.get(ClientResponse.class);
		 
		System.out.println("response: " + clientResponse.getEntity(String.class));
		assertEquals(Response.Status.OK.toString(), clientResponse.getStatusInfo().toString());
	}	

	/**
	 * Test post form
	 */
	@Test
	public void testAdd() {
		Form form = new Form();
		form.add("name", "Philip");
		form.add("age", "41");
		
		clientResponse = webResource.path("user/add")
				.accept(MediaType.TEXT_PLAIN)
				.post(ClientResponse.class, form);
		 
		System.out.println("response: " + clientResponse.getEntity(String.class));
		assertEquals(Response.Status.OK.toString(), clientResponse.getStatusInfo().toString());
	}	
}

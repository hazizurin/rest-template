package my.com.infopro.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import my.com.infopro.rest.bean.User;

@Path("/user")
public class UserResource {	
	@GET
	@Produces( { MediaType.TEXT_PLAIN } )
	public Response welcome() {
		return Response.status(Status.OK).entity("Welcome User").build();
	}

	@GET
	@Path("/age")
	@Produces( { MediaType.TEXT_PLAIN } )
	public Response sendMessage(@QueryParam("birth") String birth) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		Date birthDate = null;
		try {
			birthDate = sdf.parse(birth);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Calendar birthDay = Calendar.getInstance();
		birthDay.setTimeInMillis(birthDate.getTime());
		
		Calendar now = Calendar.getInstance();
		int years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
	      
		return Response.status(Status.OK).entity("Age (years): " + years).build();
	}

	@GET
	@Path("/profile")
	@Produces( {MediaType.APPLICATION_JSON } )
	public Response profile() {
		User user = new User();
		user.setUserName("Kruise");
		user.setLocation("Kajang");

		return Response.status(Status.OK).entity(user).build();
	}

	@POST
	@Path("/add")
	@Produces( { MediaType.TEXT_PLAIN } )
	public Response register(
			@FormParam("name") String name,
			@FormParam("age") int age) {
		return Response.status(Status.OK).entity("Added: " + name + ", " + age).build();
	}

	@POST
	@Path("/register")
	@Consumes( {MediaType.APPLICATION_JSON } )
	@Produces( {MediaType.APPLICATION_JSON } )
	public Response register(User userParam) {
		return Response.status(Status.OK).entity(userParam).build();
	}
}
